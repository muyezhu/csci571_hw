% RBF kernel
% array of lambda values
display('problem 6: gaussian kernel regression');
lambda = [1e-6, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10, 100, 1000];
sigma2 = [0.125, 0.25, 0.5, 1, 2, 4, 8];
N = size(vote, 1);
% 3 runs
for i = 1 : 3
   % random split
    p = randperm(N);
    train = vote(p(1 : ceil(N /2)), :);
    test = vote(p(ceil(N / 2) + 1 : end), :);
    % normalize
    X_train = train(:, 2 : 7);
    X_train_mean = repmat(nanmean(X_train, 1), size(train, 1), 1);
    X_train_std = repmat(nanstd(X_train, 1), size(train, 1), 1);
    X_train = (X_train - X_train_mean) ./ X_train_std;
    X_train_norm = [ones(size(X_train, 1), 1), X_train(:, 1 : 6)];
    y_train = train(:, 1);
    X_test = test(:, 2 : 7);
    X_test = (X_test - X_train_mean(1: end -1,:)) ./ X_train_std(1 : end - 1, :);
    X_test_norm = [ones(size(X_test, 1), 1), X_test(:, 1 : 6)];
    y_test = test(:, 1);
    clear('train', 'test');
    % 5 fold cross validation 
    n_cv = ceil(size(X_train, 1) / 5);
    lambda_sigma_cv_rss = zeros(9, 7);
    for j = 1 : 5
        % construct cross validation data sets.
        test_cv = X_train_norm((j - 1) * n_cv +1 : min(j * n_cv, end), :);
        y_test_cv = y_train((j - 1) * n_cv +1 : min(j * n_cv, end), :);
        if j == 1
           train_cv = X_train_norm(n_cv + 1 : end, :) ;
           y_train_cv = y_train(n_cv + 1 : end, :) ;
        elseif j == 5
           train_cv = X_train_norm(1 : 4 * n_cv, :); 
           y_train_cv = y_train(1 : 4 * n_cv, :); 
        else
           train_cv = X_train_norm([1 : (j - 1) * n_cv, j * n_cv + 1 : end ], :);
           y_train_cv = y_train([1 : (j - 1) * n_cv, j * n_cv + 1 : end ], :);
        end  
        % construct kernel
        % 1. kl2: pair wise L2 distance between samples
        % k12 = repmat(sum(X .* X, 2)) + repmat(sum(transpose(X .* X),1)) - 2X * transpose(X)
        k12ii = repmat(sum(train_cv .* train_cv, 2), 1, size(train_cv, 1));
        kl2jj = repmat(sum(transpose(train_cv) .* transpose(train_cv), 1), size(train_cv, 1), 1);
        kl2ij = train_cv * transpose(train_cv);
        kl2 = k12ii + kl2jj - 2 * kl2ij;
        % kappa
        kappal2ii = repmat(sum(train_cv .* train_cv, 2), 1, size(test_cv, 1));
        kappal2jj = repmat(sum(transpose(test_cv) .* transpose(test_cv), 1), size(train_cv, 1), 1);
        kappal2ij = train_cv * transpose(test_cv);
        kappal2 = kappal2ii + kappal2jj - 2 * kappal2ij;
        % for each lamda and sigma2, calculate rss
        for k = 1 : 9
           for h = 1 : 7
               % K = exp(-kll/sigma2)
               K = exp(- kl2 / sigma2(h));
               kappa = exp(- kappal2 / sigma2(h));
               H_cv = transpose(y_train_cv)/(K + lambda(k) * eye(size(K, 1))) * kappa;
               H_cv = transpose(H_cv);
               lambda_sigma_cv_rss(k, h) = sum((y_test_cv - H_cv).^2);
           end
        end
    end
    min_rss = lambda_sigma_cv_rss(1, 1);
    Ilambda = 1;
    Isigma = 1;
    for lambdai = 1 : size(lambda_sigma_cv_rss, 1)
        for sigmaj = 1 : size(lambda_sigma_cv_rss, 2)
            if min_rss > lambda_sigma_cv_rss(lambdai, sigmaj)
               min_rss = lambda_sigma_cv_rss(lambdai, sigmaj);
               Ilambda = lambdai;
               Isigma = sigmaj;
            end
        end
    end
    l = lambda(Ilambda);
    s = sigma2(Isigma);
    str = strcat('Run ', num2str(i), ': optimal lambda = ', num2str(l), ', sigma^2 = ', num2str(s));
    fprintf('%s\t', str);
    % use selected lambda on test data.
    k12ii = repmat(sum(X_train_norm .* X_train_norm, 2), 1, size(X_train_norm, 1));
    kl2jj = repmat(sum(transpose(X_train_norm) .* transpose(X_train_norm), 1), size(X_train_norm, 1), 1);
    kl2ij = X_train_norm * transpose(X_train_norm);
    kl2 = k12ii + kl2jj - 2 * kl2ij;
    K = exp(- kl2 / s);
    kappal2ii = repmat(sum(X_train_norm .* X_train_norm, 2), 1, size(X_test_norm, 1));
    kappal2jj = repmat(sum(transpose(X_test_norm) .* transpose(X_test_norm), 1), size(X_train_norm, 1), 1);
    kappal2ij = X_train_norm * transpose(X_test_norm);
    kappal2 = kappal2ii + kappal2jj - 2 * kappal2ij;
    kappa = exp(- kappal2 / s);
    H_test = transpose(y_train) / (K + l * eye(size(K, 1))) * kappa;
    H_test = transpose(H_test);
    test_rss_average = sum((y_test - H_test).^2) / size(y_test, 1);
    str = strcat('Average test rss = ', num2str(test_rss_average));
    fprintf('%s\n', str);
end

clear('lambda','lambda_cv_rss','str','N','p','l','m','H_test','test_rss_average','H_cv','train_cv','y_train_cv','X_train_norm','X_test_norm');
clear('X_train_mean','X_train_std','n_cv', 'K','kappa', 'test_cv', 'y_test', 'y_test_cv','i','j','k','lambda_sigma_cv_rss');



























