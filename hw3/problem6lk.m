% linear kernel. K = transpose(X)*X
% array of lambda values
display('problem 6: linear kernel regression');
lambda = [1e-6, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10, 100, 1000];
N = size(vote, 1);
% 3 runs
for i = 1 : 3
    % random split
    p = randperm(N);
    train = vote(p(1 : ceil(N /2)), :);
    test = vote(p(ceil(N / 2) + 1 : end), :);
    % normalize
    X_train = train(:, 2 : 7);
    X_train_mean = repmat(nanmean(X_train, 1), size(train, 1), 1);
    X_train_std = repmat(nanstd(X_train, 1), size(train, 1), 1);
    X_train = (X_train - X_train_mean) ./ X_train_std;
    X_train_norm = [ones(size(X_train, 1), 1), X_train(:, 1 : 6)];
    y_train = train(:, 1);
    X_test = test(:, 2 : 7);
    X_test = (X_test - X_train_mean(1: end -1,:)) ./ X_train_std(1 : end - 1, :);
    X_test_norm = [ones(size(X_test, 1), 1), X_test(:, 1 : 6)];
    y_test = test(:, 1);
    clear('train', 'test');
    % 5 fold cross validation 
    n_cv = ceil(size(X_train, 1) / 5);
    lambda_cv_rss = zeros(1, 9);
    for j = 1 : 5
        % construct cross validation data sets.
        test_cv = X_train_norm((j - 1) * n_cv +1 : min(j * n_cv, end), :);
        y_test_cv = y_train((j - 1) * n_cv +1 : min(j * n_cv, end), :);
        if j == 1
           train_cv = X_train_norm(n_cv + 1 : end, :) ;
           y_train_cv = y_train(n_cv + 1 : end, :) ;
        elseif j == 5
           train_cv = X_train_norm(1 : 4 * n_cv, :); 
           y_train_cv = y_train(1 : 4 * n_cv, :); 
        else
           train_cv = X_train_norm([1 : (j - 1) * n_cv, j * n_cv + 1 : end ], :);
           y_train_cv = y_train([1 : (j - 1) * n_cv, j * n_cv + 1 : end ], :);
        end  
        % construct kernel
        K = train_cv * transpose(train_cv);
        kappa = train_cv * transpose(test_cv);
        % in kernel ridge regression, transpose(Y) and K is obtained from
        % training data. Kappa is obtained from kernel function between
        % training and testing data, as X_train * transpose(X_test). At end
        % of the following equation of prediction, predicted values are in
        % a row.
        for k = 1 : 9
           % for each lambda, calculate rss in train_cv
           % safely use inv(): K + lambda * I is PSD
           H_cv = transpose(y_train_cv)/(K + lambda(k) * eye(size(K, 1))) * kappa;
           H_cv = transpose(H_cv);
           lambda_cv_rss(k) = sum((y_test_cv - H_cv).^2);
        end
    end
    % identify lambda that minimizes testing error during cross validation
    [m, I] = min(lambda_cv_rss);
    l = lambda(I);
    str = strcat('Run ', num2str(i), ': optimal lambda = ', num2str(l));
    fprintf('%s\t', str);
    % use selected lambda on test data.
    K = X_train_norm * transpose(X_train_norm);
    kappa = X_train_norm * transpose(X_test_norm);
    H_test = transpose(y_train) / (K + l * eye(size(K, 1))) * kappa;
    H_test = transpose(H_test);
    test_rss_average = sum((y_test - H_test).^2) / size(y_test, 1);
    str = strcat('Average test rss = ', num2str(test_rss_average));
    fprintf('%s\n', str);
end
clear('lambda','lambda_cv_rss','str','N','p','l','m','H_test','test_rss_average','H_cv','train_cv','y_train_cv','X_train_norm','X_test_norm');
clear('X_train_mean','X_train_std','n_cv', 'K','kappa', 'test_cv', 'y_test', 'y_test_cv','i','j','k','lambda_rss');
