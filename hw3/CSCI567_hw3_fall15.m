% 5a
% sum of squares for 100 data sets.
SS1 = zeros(100, 1);
SS2 = zeros(100, 1);
SS3 = zeros(100, 1);
SS4 = zeros(100, 1);
SS5 = zeros(100, 1);
SS6 = zeros(100, 1);
% bias^2 for 100 data sets and 6 g functions.
% y and g(x)s across all datasets
G0 = zeros(10, 6);
var0 = zeros(100, 6);
bias0 = zeros(100, 6);

% x: drawn from uniform [-1, 1]; y = f(x) + e, e is from gaussian
for n = 1 : 100
    singular = true;
    % check XtX is invertible
    while singular
        % sample x 10 times uniformly in [-1, 1].
        X = unifrnd(-1, 1, 10, 1);
        % x ^ 2
        XX = X .* X;
        % x ^ 3
        XXX = XX .* X;
        % x ^ 4
        XXXX = XXX .* X;
        if det(transpose(X)*X)~=0 && det(transpose(XX)* XX) ~= 0 && det(transpose(XXX) * XXX) ~= 0 && det(transpose(XXXX) * XXXX) ~= 0
            singular = false;
        end
    end
    % sample e 10 times from N(0, 0.1).
    e = normrnd(0, 0.1, 10, 1);
    % y = 2x^2 + e.
    Y = 2 * XX + e;
    Y_true = 2 * XX;
    % feature matrices for g1 to g6
    F1 = X;
    F2 = X;
    F3 = [ones(10, 1), X];
    F4 = [F3, XX];
    F5 = [F4, XXX];
    F6 = [F5, XXXX];
    
    % fit model analytically. g1 doesnt need to be fit, only need bias and var calculation.
    % w: weight vector. F: feteature matrix. H: prediction vector, aka
    % g(x)vector. G0: total g(x) values from all data sets.
    H1 = ones(10, 1);
    G0(:, 1) = H1;
    w2 = mean(Y);
    H2 = w2 * ones(10, 1);
    G0(:, 2) = H2;
    w3 = (transpose(F3) * F3) \ transpose(F3) * Y;
    H3 = F3 * w3;
    G0(:, 3) = H3;
    w4 = (transpose(F4) * F4) \ transpose(F4) * Y;
    H4 = F4 * w4;
    G0(:, 4) = H4;
    w5 = (transpose(F5) * F5) \ transpose(F5) * Y;
    H5 = F5 * w5;
    G0(:, 5) = H5;
    w6 = (transpose(F6) * F6) \ transpose(F6) * Y;
    H6 = F6 * w6;
    G0(:, 6) = H6;
    Egx = mean(G0, 1);
    Egxmat = repmat(Egx, 10, 1);
    var0(n, :) = nanmean((G0 - Egxmat).^2, 1);
    Y_true = repmat(Y_true, 1, 6);
    bias0(n, :) = nanmean((Y_true - G0).^2);
    % compute SS 
    SS1(n) = sum((H1 - Y).^ 2);
    SS2(n) = sum((H2 - Y).^ 2);
    SS3(n) = sum((H3 - Y).^ 2);
    SS4(n) = sum((H4 - Y).^ 2);
    SS5(n) = sum((H5 - Y).^ 2);
    SS6(n) = sum((H6 - Y).^ 2);
end
% plot histogram.
figure;
% SS for each data set. 100 sets in total, 10 data per set.
SS = [SS1, SS2, SS3, SS4, SS5, SS6];
MSE = SS / 10;
for i = 1 : 6
   subplot(3, 2, i);
   hist(MSE(:, i));
   title(strcat('g', num2str(i)));
end
% calculate bias and variance across all data sets.
variance = nanmean(var0,1);
Ebias = nanmean(bias0,1);

display('problem 5a');
display('sample size = 10, 100 data sets, no regularization:');
for i = 1 : 6
   str = strcat('g', num2str(i)); 
   fprintf('%s\t', str);
   str = strcat('variance = ', num2str(variance(i)));
   fprintf('%s, ', str);
   str = strcat('bias^2 = ', num2str(Ebias(i)));
   fprintf('%s\n', str);
end

% 5b
% sum of squares for 100 data sets.
SS1 = zeros(100, 1);
SS2 = zeros(100, 1);
SS3 = zeros(100, 1);
SS4 = zeros(100, 1);
SS5 = zeros(100, 1);
SS6 = zeros(100, 1);
bias2 = zeros(100, 6);
% y and g(x)s across all datasets
G0 = zeros(100, 6);
var0 = zeros(100, 6);
bias0 = zeros(100, 6);

% x: drawn from uniform [-1, 1]; y = f(x) + e, e is from gaussian
F40 = zeros(10000, 3);
for n = 1 : 100
    singular = true;
    % check XtX is invertible
    while singular
        % sample x 100 times uniformly in [-1, 1].
        X = unifrnd(-1, 1, 100, 1);
        % x ^ 2
        XX = X .* X;
        % x ^ 3
        XXX = XX .* X;
        % x ^ 4
        XXXX = XXX .* X;
        if det(transpose(X)*X)~=0 && det(transpose(XX)* XX) ~= 0 && det(transpose(XXX) * XXX) ~= 0 && det(transpose(XXXX) * XXXX) ~= 0
            singular = false;
        end
    end
    % sample e 100 times from N(0, 0.1).
    e = normrnd(0, 0.1, 100, 1);
    % y = 2x^2 + e.
    Y = 2 * XX + e;
    Y_true = 2 * XX;
    % feature matrices for g1 to g6
    F1 = X;
    F2 = X;
    F3 = [ones(100, 1), X];
    F4 = [F3, XX];
    F40((n - 1) * 100 + 1 : n * 100, :) = F4;
    F5 = [F4, XXX];
    F6 = [F5, XXXX];
    
    % fit model analytically. g1 doesnt need to be fit, only need bias and var calculation.
    % w: weight vector. F: feteature matrix. H: prediction vector, aka
    % g(x)vector. G0: total g(x) values from all data sets.
    H1 = ones(100, 1);
    G0(:, 1) = H1;
    w2 = mean(Y);
    H2 = w2 * ones(100, 1);
    G0(:, 2) = H2;
    w3 = (transpose(F3) * F3) \ transpose(F3) * Y;
    H3 = F3 * w3;
    G0(:, 3) = H3;
    w4 = (transpose(F4) * F4) \ transpose(F4) * Y;
    H4 = F4 * w4;
    G0(:, 4) = H4;
    w5 = (transpose(F5) * F5) \ transpose(F5) * Y;
    H5 = F5 * w5;
    G0(:, 5) = H5;
    w6 = (transpose(F6) * F6) \ transpose(F6) * Y;
    H6 = F6 * w6;
    G0(:, 6) = H6;
    Egx = mean(G0, 1);
    Egxmat = repmat(Egx, 100, 1);
    var0(n, :) = nanmean((G0 - Egxmat).^2, 1);
    Y_true = repmat(Y_true, 1, 6);
    bias0(n, :) = nanmean((Y_true - G0).^2);
    
    % compute SS 
    SS1(n) = sum((H1 - Y).^ 2);
    SS2(n) = sum((H2 - Y).^ 2);
    SS3(n) = sum((H3 - Y).^ 2);
    SS4(n) = sum((H4 - Y).^ 2);
    SS5(n) = sum((H5 - Y).^ 2);
    SS6(n) = sum((H6 - Y).^ 2);
end
% calculate bias and variance across all data sets.
variance = nanmean(var0, 1);
Ebias = nanmean(bias0, 1);
%noise = sum((Ey - Y0).^2, 1)/1000;
% plot histogram.
figure;
% SS for each data set. 100 sets in total, 100 data points each data set.
SS = [SS1, SS2, SS3, SS4, SS5, SS6];
MSE = SS / 100;
for i = 1 : 6
   subplot(3, 2, i);
   hist(MSE(:, i));
   title(strcat('g', num2str(i)));
end
display('problem 5b');
display('sample size = 100, 100 data sets, no regularization:');
for i = 1 : 6
   str = strcat('g', num2str(i)); 
   fprintf('%s\t', str);
   str = strcat('variance = ', num2str(variance(i)));
   fprintf('%s, ', str);
   str = strcat('bias^2 = ', num2str(Ebias(i)));
   fprintf('%s\n', str);
end

% 5d. L2 regularized g4. F40: design matrix from all datasets for g4
% G4reg: predicted values with regularization, 1000 x 4, for 4 lamdas
G4reg = zeros(100, 4);
bias0_reg = ones(100, 4);
var0_reg = zeros(100, 4);
% lamda = 0.01
lambda = [0.01, 0.1, 1, 10];
for i = 1 : 100
    % loop through lamda values
    for j = 1 : 4
        % F4: 100 x 3 dataset
        F4 = F40((i - 1) * 100 + 1 : i * 100, :);
        w4 = (transpose(F4) * F4 + lambda(j) * eye(3)) \ transpose(F4) * Y;
        G4reg (:, j) = F4 * w4;
        Egx_reg = mean(G4reg, 1);
        Egx_regmat = repmat(Egx_reg, 100, 1);
        var0_reg(i, :) = nanmean((G4reg - Egx_regmat).^2, 1);
        Y_true = Y_true(:, 1);
        Y_true = repmat(Y_true, 1, 4);
        bias0_reg(i, :) = nanmean((Y_true - Egx_regmat).^2);
    end
end

variance_reg = nanmean(var0_reg);

Ebias2_reg = nanmean(bias0_reg);

display('problem 5d');
display('sample size = 100, 100 data sets, with L2 regularization:');
for i = 1 : 4
   str = strcat('lambda = ', num2str(lambda(i))); 
   fprintf('%s\t', str);
   str = strcat('variance = ', num2str(variance_reg(i)));
   fprintf('%s, ', str);
   str = strcat('bias^2 = ', num2str(Ebias2_reg(i)));
   fprintf('%s\n', str);
end
%clear;

vote = xlsread('vote.xlsx', 'A1:G3107');
run('problem6lr.m');
run('problem6lk.m');
run('problem6pk.m');
run('problem6RBF.m');


















