% polynomial kernel
display('problem 6: polynomial kernel regression');
% arrays of lambda, a, c values
lambda = [1e-6, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10, 100, 1000];
a = [-1, -0.5, 0, 0.5, 1];
c = [1, 2, 3, 4];
N = size(vote, 1);
% 3 runs
for i = 1 : 3
   % random split
    p = randperm(N);
    train = vote(p(1 : ceil(N /2)), :);
    test = vote(p(ceil(N / 2) + 1 : end), :);
    % normalize
    X_train = train(:, 2 : 7);
    X_train_mean = repmat(nanmean(X_train, 1), size(train, 1), 1);
    X_train_std = repmat(nanstd(X_train, 1), size(train, 1), 1);
    X_train = (X_train - X_train_mean) ./ X_train_std;
    X_train_norm = [ones(size(X_train, 1), 1), X_train(:, 1 : 6)];
    y_train = train(:, 1);
    X_test = test(:, 2 : 7);
    X_test = (X_test - X_train_mean(1: end -1,:)) ./ X_train_std(1 : end - 1, :);
    X_test_norm = [ones(size(X_test, 1), 1), X_test(:, 1 : 6)];
    y_test = test(:, 1);
    clear('train', 'test');
    % 5 fold cross validation 
    n_cv = ceil(size(X_train, 1) / 5);
    % 3D rss array, lambda, a, c
    lambda_a_c_rss(:, :, 4) = zeros(9, 5);
    for j = 1 : 5
        % construct cross validation data sets.
        test_cv = X_train_norm((j - 1) * n_cv +1 : min(j * n_cv, end), :);
        y_test_cv = y_train((j - 1) * n_cv +1 : min(j * n_cv, end), :);
        if j == 1
           train_cv = X_train_norm(n_cv + 1 : end, :) ;
           y_train_cv = y_train(n_cv + 1 : end, :) ;
        elseif j == 5
           train_cv = X_train_norm(1 : 4 * n_cv, :); 
           y_train_cv = y_train(1 : 4 * n_cv, :); 
        else
           train_cv = X_train_norm([1 : (j - 1) * n_cv, j * n_cv + 1 : end ], :);
           y_train_cv = y_train([1 : (j - 1) * n_cv, j * n_cv + 1 : end ], :);
        end
        K_ =  train_cv * transpose(train_cv);
        kappa_ = train_cv * transpose(test_cv);
        % search parameter space
        for k = 1 : 9
           for s = 1 : 5
               for t = 1 : 4
                   % construct kernel and kappa
                   K = (K_ + a(s)) .^ c(t);
                   kappa = (kappa_ + a(s)) .^ c(t);
                   H_cv = transpose(y_train_cv)/(K + lambda(k) * eye(size(K, 1))) * kappa;
                   H_cv = transpose(H_cv);
                   lambda_a_c_rss(k, s, t) = sum((y_test_cv - H_cv).^2);
               end
           end
        end
    end
    min_rss = lambda_a_c_rss(1, 1, 1);
    Ilambda = 1;
    Ia = 1;
    Ic = 1;
    for k = 1 : 9
       for s = 1 : 5;
           for t = 1 : 4
               if min_rss > lambda_a_c_rss(k, s, t)
                  min_rss = lambda_a_c_rss(k, s, t);
                  Ilambda = k;
                  Ia = s;
                  Ic = t;
               end
           end    
       end
    end
    l = lambda(Ilambda);
    a_opt = a(Ia);
    c_opt = c(Ic);
    str = strcat('Run ', num2str(i), ': optimal lambda = ', num2str(l), ', a = ', num2str(a_opt),', c = ', num2str(c_opt));
    fprintf('%s\t', str);
    K =  (X_train_norm * transpose(X_train_norm) + a_opt) .^ c_opt;
    kappa = (X_train_norm * transpose(X_test_norm) + a_opt) .^ c_opt;
    H_test = transpose(y_train) / (K + l * eye(size(K, 1))) * kappa;
    H_test = transpose(H_test);
    test_rss_average = sum((y_test - H_test).^2) / size(y_test, 1);
    str = strcat('Average test rss = ', num2str(test_rss_average));
    fprintf('%s\n', str);
end




















