% linear ridge regression
% [ln(VOTES/POP) POP EDUCATION HOUSES INCOME XCOORD YCOORD] 
display('problem 6: linear regression');
lambda = [0, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10, 100, 1000];
% error on 3 runs for 8 lambda values
N = size(vote, 1);
W = zeros(3, 7);
% 50/50 split, 3 runs
for i = 1 : 3
    % random split
    p = randperm(N);
    train = vote(p(1 : ceil(N /2)), :);
    test = vote(p(ceil(N / 2) + 1 : end), :);
    % normalize
    X_train = train(:, 2 : 7);
    X_train_mean = repmat(nanmean(X_train, 1), size(train, 1), 1);
    X_train_std = repmat(nanstd(X_train, 1), size(train, 1), 1);
    y_train = train(:, 1);
    X_train = (X_train - X_train_mean) ./ X_train_std;
    X_train_norm = [ones(size(X_train, 1), 1), X_train(:, 1 : 6)];
    X_test = test(:, 2 : 7);
    X_test = (X_test - X_train_mean(1: end -1,:)) ./ X_train_std(1 : end - 1, :);
    X_test_norm = [ones(size(X_test, 1), 1), X_test(:, 1 : 6)];
    y_test = test(:, 1);
    clear('train', 'test');
    % 5 fold cross validation
    n_cv = ceil(size(X_train, 1) / 5);
    lambda_cv_rss = zeros(1, 9);
    for j = 1 : 5
        test_cv = X_train_norm((j - 1) * n_cv +1 : min(j * n_cv, end), :);
        y_test_cv = y_train((j - 1) * n_cv +1 : min(j * n_cv, end), :);
        if j == 1
           train_cv = X_train_norm(n_cv + 1 : end, :) ;
           y_train_cv = y_train(n_cv + 1 : end, :) ;
        elseif j == 5
           train_cv = X_train_norm(1 : 4 * n_cv, :); 
           y_train_cv = y_train(1 : 4 * n_cv, :); 
        else
           train_cv = X_train_norm([1 : (j - 1) * n_cv, j * n_cv + 1 : end ], :);
           y_train_cv = y_train([1 : (j - 1) * n_cv, j * n_cv + 1 : end ], :);
        end
        for k = 1 : 9
            % compute x for each lambda on training set.
            w = (transpose(train_cv) * train_cv + lambda(k) * eye(size(train_cv, 2))) \ transpose(train_cv) * y_train_cv;
            % compute RSS for lambda on testing set.
            % prediction matrix
            H_cv = test_cv * w;
            lambda_cv_rss(k) = lambda_cv_rss(k) + sum((y_test_cv - H_cv).^2) / size(test_cv, 1);
        end
    end
    % find lambda that minimize rss in cross validation testing portion.
    [m, I] = min(lambda_cv_rss);
    l = lambda(I);
    str = strcat('Run ', num2str(i), ': optimal lambda = ', num2str(l));
    fprintf('%s\t', str);
    % use all training samples and l to train w.
    w = (transpose(X_train_norm) * X_train_norm + l * eye(size(X_train_norm, 2))) \ transpose(X_train_norm) * y_train;
    W(i, :) = w;
    H_test = X_test_norm * w;
    test_rss_average = sum((y_test - H_test).^2) / size(y_test, 1);
    str = strcat('Average test rss = ', num2str(test_rss_average));
    fprintf('%s\n', str);
end

clear('lambda','lambda_cv_rss','str','N','W','p','w','l','m','H_test','test_rss_average','H_cv','train_cv','y_train_cv','X_train_norm','X_test_norm');
clear('X_train_mean','X_train_std','n_cv','test_cv', 'y_test', 'y_test_cv','i','j','k','lambda_rss');













