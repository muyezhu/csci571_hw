% problem 4a
[titanic_num, titanic_text, titanic_raw] = xlsread('titanic3.xls', 'A2:N1310');
[rows, cols] = size(titanic_raw);
missing = zeros(1, cols);
for j = 1 : cols
    col_missing = 0;
    for i = 1 : rows
        % for cell type, isnan() returns a vector thats equal in length to
        % the string in cell. for empty cell isnan() returns 1.
        if (isnan(titanic_raw{i,j}(1)))
            col_missing = col_missing + 1;
        end
    end
    missing(j) = col_missing;
end
% print answer to console.
col_names = {'pclass';'survived';'name';'sex';'age';'sibsp';'parch';'ticket';'fare';'cabin';'embarked';'boat';'body';'home.dest'};
display('Number of missing values in each columns are:');
for i = 1 : cols
   str = strcat(col_names{i}, ':   ', num2str(missing(i)));
   fprintf('%s\n', str);
end
clear('col_missing','missing');

% shuffle data
display('Shuffling data...');
p = randperm(rows);
titanic_perm = titanic_raw;
for i = 1 : rows
   titanic_perm(i , :) = titanic_raw(p(i), :); 
end
% move survival to column 1 position
temp = titanic_perm(: , 1);
titanic_perm(: , 1) = titanic_perm(: , 2);
titanic_perm(: , 2) = temp;
titanic_train = titanic_perm(1 : int32(rows / 2) , :);
titanic_train_data = titanic_train(: , 2 : end);
titanic_train_survival = cell2mat(titanic_train(: , 1));
titanic_test = titanic_perm(int32(rows / 2) + 1 : end, :);
titanic_test_data = titanic_test(: , 2 : end);
titanic_test_survival = cell2mat(titanic_test(: , 1));
display('Data shuffled.');
fprintf('Survival column separated.\n\n');
clear('p','titanic_perm');

% problem 4b
pclass = cell2mat(titanic_train(: , 2));
age = cell2mat(titanic_train(: , 5));
sibsp = cell2mat(titanic_train(: , 6));
parch = cell2mat(titanic_train(: , 7));
fare = cell2mat(titanic_train(: , 9));
numeric_train = [pclass, age,sibsp, parch,fare];
numeric_train_titles = {'pclass', 'age', 'sibsp', 'parch', 'fare'};
figure;
for i = 1 : 5
    current_col = ceil(numeric_train(:, i));
    current_range = range(current_col);
    min_val = min(current_col);
    max_val = max(current_col) + 1;
    % descritize. each bin covers value [init, init + width).
    if (length(unique(current_col)) < 10)
        width = 1;
        bins = min_val : width : (max_val - width);
    else
        width = floor(current_range / 10) + 1;
        bins = min_val : width : min_val + 9 * width;
    end
    % initialize counts within each bins.
    bin_counts = zeros(1, length(bins)); 
    bin_total = zeros(1, length(bins));
    % calculates the number of survival in each bin    
    for j = 1 : length(current_col)
        if (~(isnan(current_col(j))))
            offset = floor((current_col(j) - min_val) / width);
            % increment count in appropriate bin
            if (titanic_train_survival(j) == 1)
                bin_counts(1 + offset) = bin_counts(1 + offset) + 1; 
            end
            bin_total(1 + offset) = bin_total(1 + offset) + 1;
        end    
    end
    % calculate probablity of survival in each bin.
    bin_counts = bin_counts ./ bin_total;
    % plot bar graph
    subplot(3,2,i);
    x_label = 1 : length(bins);
    bar(x_label, bin_counts, 1);
    title(numeric_train_titles{i});
end
clear('x_label','bin_counts','width','bins','bin_total','current_col','current_range','min_val','max_val');

%problem 4c
% 7 independent variables of interest.
% for ease of processing, represent categorical values as dummy variable.
% male:1. female: 0.
t3 = zeros(length(titanic_train_survival), 1);
for i = 1 : length(t3)
    if (titanic_train_data{i, 3}(1) == 'm')
        t3(i) = 1;
    end
end
% C: 1. Q: 2. S: 3.
t10 = zeros(length(titanic_train_survival), 1);
for i = 1 : length(t10)
    if (titanic_train_data{i, 10}(1) == 'C')
        t10(i) = 1;
    elseif (titanic_train_data{i, 10}(1) == 'Q')
        t10(i) = 2;
    elseif (titanic_train_data{i, 10}(1) == 'S')
        t10(i) =3;
    else
        t10(i) = NaN;
    end
end
% extract columns for 4c analysis into matrix mat_h.
temp_indices = [1,4,5,6,8];
mat_h_indices = [1,3,4,5,6];
mat_h = zeros(length(titanic_train_survival), 7);
mat_h(: , 2) =  t3;
mat_h(: , 7) = t10;
for i = 1 : length(temp_indices)
    t = cell2mat(titanic_train_data(: , temp_indices(i)));
   mat_h(:,mat_h_indices(i)) = t;
end
% co-sort survival and one variable at a time
mutual_info = zeros(1 , 7);
variables = {'pclass', 'sex', 'age', 'sibsp', 'parch', 'fare', 'embarked'};
for i = 1 : 7
   [val_sorted, indices] = sort(mat_h(: , i));
   survival_sorted = titanic_train_survival(indices);
   % calculate total entropy
   total_size = length(val_sorted) - sum(isnan(val_sorted));
   total_survival_count = sum(survival_sorted(1 : total_size));
   p_total_survival = total_survival_count / total_size;
   p_total = [p_total_survival, 1 - p_total_survival];
   H_total = - sum(p_total .* log(p_total));
   % split non-NaN values into equal density bins
   bin_size = floor(total_size / 10);
   final_bin_size = total_size - 9 * bin_size;
   bin_init_indices = 1 : bin_size : 1 + 9 * bin_size;
   % calculate conditional entropy
   H_bin = zeros(10 , 1);
   occurance_bin = zeros(10, 1);
   for j = 1 : 9
       bin_survival_count = sum(survival_sorted(bin_init_indices(j) : bin_init_indices(j + 1) - 1) == 1);
       p_bin_survival = bin_survival_count / bin_size;
       p_bin = [p_bin_survival, 1 - p_bin_survival];
       H_bin(j) = -sum(p_bin .* log(p_bin));
       occurance_bin(j) = bin_size / total_size;
   end    
   bin_survival_count_final = sum(survival_sorted(bin_init_indices(10) : total_size) == 1);
   p_bin_survival_final = bin_survival_count_final / final_bin_size;
   p_bin_final = [p_bin_survival_final, 1 - p_bin_survival_final];
   H_bin(10) = - sum(p_bin_final .* log(p_bin_final));
   occurance_bin(10) = final_bin_size / total_size;
   H_conditional = sum(H_bin .* occurance_bin);
   % calculate mutual information
   mutual_info(i) = H_total - H_conditional;
end

% sort mutual information
[mutual_info_sorted, indices_mi] = sort(mutual_info, 'descend');
variables_sorted = variables(indices_mi);
display('Mutual information sorted in descending order: ');
for i = 1 : length(mutual_info_sorted)
   str = strcat(variables_sorted{i}, ':   ', num2str(mutual_info_sorted(i)));
   fprintf('%s\n', str);
end
fprintf('\n');
clear('val_sorted','bin_survival_count','p_bin_survival','p_bin','occurance_bin','bin_survival_count_final','p_bin_survival_final','p_bin_final','H_conditional','mutual_info','H_bin');
clear('str','indices_mi','indices','variables_sorted','variables');

% problem 4d
% substitute missing values as indicated in training data
fare_sub = mat_h(:, 6);
fare_sub(isnan(fare_sub) == 1) = 33.2955;
% dummy code embarked_c, embarked_q. s is omitted to avoid mutilinearity.
% pclass does not need to be considered categorical. 
embarked_c = zeros(length(titanic_train_survival), 1);
embarked_q = zeros(length(titanic_train_survival), 1);
embarked_c (t10 == 1) = 1;
embarked_q (t10 == 2) = 1;
X_train = [pclass, mat_h(:, 2 : 5), fare_sub, embarked_c, embarked_q];
BE_train = X_train;

% process the test data set as needed
pclass_test = cell2mat(titanic_test(: , 2));
age_test = cell2mat(titanic_test(: , 5));
sibsp_test = cell2mat(titanic_test(: , 6));
parch_test = cell2mat(titanic_test(: , 7));
fare_test = cell2mat(titanic_test(: , 9));
fare_test(isnan(fare_test) == 1) = 33.2955;
sex_test = zeros(length(titanic_test_survival), 1);
embarked_c_test = zeros(length(titanic_test_survival), 1);
embarked_q_test = zeros(length(titanic_test_survival), 1);
for i = 1 : length(titanic_test_survival)
    if (titanic_test_data {i, 3}(1) == 'm')
        sex_test(i) = 1;
    end
    if (titanic_test_data{i, 10}(1) == 'C')
        embarked_c_test(i) = 1;
    end
    if (titanic_test_data{i, 10}(1) == 'Q')
        embarked_q_test(i) = 1;
    end
end
% variabes: 'pclass', 'sex', 'age', 'sibsp', 'parch', 'fare', 'embarked'
X_test = [pclass_test, sex_test, age_test, sibsp_test, parch_test, fare_test, embarked_c_test, embarked_q_test];
BE_test= X_test;
% mean of features in row vector
X_train_mean = nanmean(X_train, 1);
X_train_mean_mat = repmat(X_train_mean, length(titanic_train_survival), 1);
% standard deviation of features
X_train_std = nanstd(X_train, 0, 1);
X_train_std_mat = repmat(X_train_std, length(titanic_train_survival), 1);
% normalize training data
X_train = (X_train - X_train_mean_mat) ./ X_train_std_mat;
% normalize test data
X_train_mean_mat = repmat(X_train_mean, length(titanic_test_survival), 1);
X_train_std_mat = repmat(X_train_std, length(titanic_test_survival), 1);
X_test = (X_test - X_train_mean_mat) ./ X_train_std_mat;
% multiple models
w1 = glmfit(X_train, titanic_train_survival, 'normal');
sig_hat1 = glmval(w1, X_test, 'identity');
sig_hat1_train = glmval(w1, X_train, 'identity');
y_hat1 = zeros(length(titanic_test_survival), 1);
y_hat1_train = zeros(length(titanic_train_survival), 1);
y_hat1 (isnan(sig_hat1)) = NaN;
y_hat1 (sig_hat1 > 0.5) = 1;
y_hat1_train (isnan(sig_hat1_train)) = NaN;
y_hat1_train (sig_hat1_train > 0.5) = 1;
% for samples where age = NaN, 2nd model.
nan_pos = find(isnan(y_hat1));
nan_pos_train = find(isnan(y_hat1_train));
X_test_nan_pos = X_test(nan_pos, [1 : 2, 4 : end]);
X_train_nan_pos = X_train(nan_pos_train, [1 : 2, 4 : end]);
titanic_test_survival_nan_pos = titanic_test_survival(nan_pos);
titanic_train_survival_nan_pos = titanic_train_survival(nan_pos_train);
X_train_no_age = X_train(:, [1 : 2, 4 : end]);
w2 = glmfit(X_train_no_age, titanic_train_survival, 'normal');
sig_hat2 = glmval(w2, X_test_nan_pos, 'identity');
sig_hat2_train = glmval(w2, X_train_nan_pos, 'identity');
y_hat2 = zeros(length(sig_hat2), 1);
y_hat2 (sig_hat2 > 0.5) = 1;
y_hat2_train = zeros(length(sig_hat2_train), 1);
y_hat2_train (sig_hat2_train > 0.5) = 1;
% calculate model training and testing accuracy.
not_nan_indices = find(~isnan(y_hat1));
not_nan_indices_train = find(~isnan(y_hat1_train));
correct1 = zeros(length(not_nan_indices), 1);
correct1 (y_hat1(not_nan_indices) == titanic_test_survival(not_nan_indices)) = 1;
correct2 = zeros(length(nan_pos), 1);
correct2 (y_hat2 == titanic_test_survival_nan_pos) = 1;
correct_rate_multi = (sum(correct1) + sum(correct2)) / length(titanic_test_survival);
correct1_train = zeros(length(not_nan_indices_train), 1);
correct1_train (y_hat1_train(not_nan_indices_train) == titanic_train_survival(not_nan_indices_train)) = 1;
correct2_train = zeros(length(nan_pos_train), 1);
correct2_train (y_hat2_train == titanic_train_survival_nan_pos) = 1;
correct_rate_multi_train = (sum(correct1_train) + sum(correct2_train)) / length(titanic_train_survival);
display('Using multiple model: ')
str1 = strcat('    training accuracy is: ', num2str(correct_rate_multi_train));
str2 = strcat('    testing accuracy is: ', num2str(correct_rate_multi));
fprintf('%s\n', str1);
fprintf('%s\n', str2);

% substituting values
age_sub_train = X_train(: , 3);
age_sub_train(isnan(age_sub_train)) = X_train_mean(3);
X_train_sub = X_train;
X_train_sub(:, 3) = age_sub_train;
age_sub_test = X_test(: , 3);
age_sub_test(isnan(age_sub_test)) = X_train_mean(3);
X_test_sub = X_test;
X_test_sub(:, 3) = age_sub_test;
% normalize
X_train_sub_mean = nanmean(X_train, 1);
X_train_sub_mean_mat = repmat(X_train_sub_mean, length(titanic_train_survival), 1);
X_train_sub_std = nanstd(X_train_sub, 0, 1);
X_train_sub_std_mat = repmat(X_train_sub_std, length(titanic_train_survival), 1);
X_train_sub = (X_train_sub - X_train_sub_mean_mat) ./ X_train_sub_std_mat;
X_train_sub_mean_mat = repmat(X_train_sub_mean, length(titanic_test_survival), 1);
X_train_sub_std_mat = repmat(X_train_sub_std, length(titanic_test_survival), 1);
X_test_sub = (X_test_sub - X_train_sub_mean_mat) ./ X_train_sub_std_mat;
% train model
w3 = glmfit(X_train_sub, titanic_train_survival, 'normal');
sig_hat3 = glmval(w3, X_test_sub, 'identity');
y_hat3 = zeros(length(sig_hat3), 1);
y_hat3 (sig_hat3 > 0.5) = 1;
sig_hat3_train = glmval(w3, X_train_sub, 'identity');
y_hat3_train = zeros(length(sig_hat3_train), 1);
y_hat3_train (sig_hat3_train > 0.5) = 1;
correct3 = sum(y_hat3 == titanic_test_survival) / length(titanic_test_survival);
correct3_train = sum(y_hat3_train == titanic_train_survival) / length(titanic_train_survival);
display('Using single model with substituted values: ')
str1 = strcat('    training accuracy is: ', num2str(correct3_train));
str2 = strcat('    testing accuracy is: ', num2str(correct3));
fprintf('%s\n', str1);
fprintf('%s\n', str2);
clear('w1','w2','w3','X_train_sub_mean','X_train_sub_mean_mat','X_train_sub_std','X_train_sub_std_mat','age_sub_train','age_sub_test','X_test_nan_pos','X_train_nan_pos');
clear('correct1','correct2','correct3','correct1_train','correct2_train','correct3_train','y_hat1','y_hat2','y_hat3','y_hat1_train','y_hat2_train','y_hat3_train');
clear('sig_hat1','sig_hat2','sig_hat3','sig_hat1_train','sig_hat2_train','sig_hat3_train','correct_rate_multi_train','correct_rate_multi');
clear('X_train_mean','X_train_mean_mat','X_train_std','X_train_std_mat','x_train_no_age','str1','str2');
clear('nan_pos','nan_pos_train','age_sub_train','age_sub_test');


% problem 4e. BE matrix obtained in 4d.
% append sqrt pclass, age, sibsp, parch, fare
BE_train_sqrt = sqrt(BE_train(:, [1, 3 : 6]));
BE_train = [BE_train, BE_train_sqrt];
% discretize 5 numeric variables to 10 equal density bins
numeric_cols = [1, 3, 4, 5,6];
bin_init_be = zeros(10, 5);
for i = 1 : length(numeric_cols)
   % 9 columns in dummy code matrix to encode 10 bins
   dummy_mat = zeros(length(BE_train(:, numeric_cols(i))), 9); 
   [sorted_col, sorted_col_indices] = sort(BE_train(:,numeric_cols(i)));
   col_size = sum(~isnan(sorted_col));
   bin_size = ceil(col_size / 10);
   bin_init_be(:, i) = 1 : bin_size : 1 + 1 + 9 * bin_size;
   t = 1 : col_size;
   % bin assignment. NaN at end
   bin_assign = NaN(length(sorted_col),1);
   bin_assign(1 : col_size) = floor((t - 1) / bin_size) + 1;
   bin_assign(col_size + 1 : end) = NaN;
   % bin assignment in unsorted column
   l = zeros(length(BE_train(:, numeric_cols(i))), 1);
   l(sorted_col_indices) = bin_assign;
   bin_assign = l;
   % fill dummy matrix accoring to bin assignment.
   for j = 1 : length(bin_assign)
        if (isnan(bin_assign(j)))
            dummy_mat(j, :) = NaN;
        end
        if (bin_assign(j) < 10)
          dummy_mat(j, bin_assign(j)) = 1;
        end
   end   
   BE_train = [BE_train, dummy_mat];
end
clear('t','dummy_mat','l','sorted_col','sorted_col_indices','col_size','bin_size');

% append interaction variables
% before interaction, BE_train has 58 cols
[rows, cols] = size(BE_train);
n_pairs = cols * (cols - 1) / 2;
BE_interaction = zeros(rows, n_pairs);
count = 0;
% tract the column number of i, j whose interactions are eventually discarded
discard_i = zeros(1, n_pairs);
discard_j = zeros(1, n_pairs);
count_discard = 0;
for i = 1 : (cols - 1)
    for j = (i + 1) : cols
        col_inter = BE_train(:, i) .* BE_train(:, j);
        unique_number = length(unique(col_inter)) - sum(isnan(col_inter));
        if (unique_number < 2)
            count_discard = count_discard + 1;
            discard_i(count_discard) = i;
            discard_j(count_discard) = j;
            continue;
        end
        count = count + 1;
        BE_interaction(:, count) = col_inter;
    end
end
temp = BE_interaction(:, 1 : count);
BE_interaction = temp;
clear('temp');
BE_train = [BE_train, BE_interaction];

% discretization and iteraction for BE_test
BE_test_sqrt = sqrt(BE_test(:, [1, 3 : 6]));
BE_test = [BE_test, BE_test_sqrt];
% bin starting indices in bin_init_be 
for i = 1 :length(numeric_cols)
    dummy_mat_test = zeros(length(BE_test(:, numeric_cols(i))), 9); 
    [sorted_col_test, sorted_col_indices] = sort(BE_test(:,numeric_cols(i)));
    col_size_test = sum(~isnan(sorted_col_test));
    bin_cursor = 1;
    bin_assign_test = NaN(length(sorted_col_test),1);
    % bin assignment in sorted column
    for j = 1 : length(sorted_col_test)
        if (j > col_size_test)
            bin_assign_test(j) = NaN;
        elseif (j >= bin_init_be(end))
            bin_assign_test(j) = 10;
        elseif (j < bin_init_be(bin_cursor + 1))
            bin_assign_test(j) = bin_cursor;
        else
            bin_cursor = bin_cursor + 1;
            bin_assign_test(j) = bin_cursor;
        end
    end
    % bin assignment in unsorted column
   l = zeros(length(BE_test(:, numeric_cols(i))), 1);
   l(sorted_col_indices) = bin_assign_test;
   bin_assign_test = l;
   % fill dummy matrix accoring to bin assignment.
   for j = 1 : length(bin_assign_test)
        if (isnan(bin_assign_test(j)))
            dummy_mat(j, :) = NaN;
        end
        if (bin_assign(j) < 10)
          dummy_mat_test(j, bin_assign(j)) = 1;
        end
   end   
   BE_test = [BE_test, dummy_mat_test];
end
clear('dummy_mat','l');

% append interactions in BE_test
% discarded interactions in discard_i and discard_j
[rows, cols] = size(BE_test);
n_pairs = cols * (cols - 1) / 2;
BE_interaction_test = zeros(rows, n_pairs);
count = 0;
discard_cursor = 1;
for i = 1 : (cols - 1)
    for j = (i + 1) : cols
        if (i == discard_i(discard_cursor) && j == discard_j(discard_cursor))
            discard_cursor = discard_cursor + 1;
            continue;
        end
        col_inter = BE_test(:, i) .* BE_test(:, j);
        count = count + 1;
        BE_interaction_test(:, count) = col_inter;
    end
end
temp = BE_interaction_test(:, 1 : count);
BE_interaction_test = temp;
BE_test = [BE_test, BE_interaction_test];
clear('temp','col_inter','n_pairs', 'discard_cursor','discard_i','discard_j','BE_interaction', 'BE_interaction_test','sorted_col_test','sorted_col_indices','bin_init_be');

%normalize
BE_train_mean = nanmean(BE_train, 1);
BE_train_std = nanstd(BE_train,0, 1);
BE_train_mean_mat = repmat(BE_train_mean, length(titanic_train_survival), 1);
BE_train_std_mat = repmat(BE_train_std, length(titanic_train_survival), 1);
BE_train_norm = (BE_train - BE_train_mean_mat) ./ BE_train_std_mat;
BE_test_mean_mat = repmat(BE_train_mean, length(titanic_test_survival), 1);
BE_test_std_mat = repmat(BE_train_std, length(titanic_test_survival), 1);
BE_test_norm = (BE_test - BE_test_mean_mat) ./ BE_test_std_mat;
clear('BE_train_mean_mat','BE_train_std_mat','BE_test_mean_mat','BE_test_std_mat');


% problem 4f
features_train = [];
features_train_index = [];
accuracy_rec = zeros(1, 10);
accuracy_rec_test = zeros(1, 10);
for i= 1 : 10
    max_accuracy = 0;
    max_accuracy_index = 0;
    [rows, cols] = size(BE_train_norm);
    y_hat = zeros(rows, 1);
    y_hat_test = zeros(rows - 1, 1);
    for j = 1 : cols
        % if the current col is a feature already selected, go to next col.
        if (any(features_train_index == j))
            continue;
        end
        % select feature with training accuracy
        features_train_append = [features_train, BE_train_norm(:, j)];
        w = glmfit(features_train_append, titanic_train_survival,'normal');
        sig_hat = glmval(w, features_train_append, 'identity');
        y_hat (sig_hat > 0.5) = 1;
        y_hat (isnan(sig_hat)) = NaN;
        accuracy = sum(y_hat == titanic_train_survival) / (rows - sum(isnan(y_hat)));
        if (accuracy > max_accuracy)
            max_accuracy = accuracy;
            max_accuracy_index = j;
            w_optimal = w;
        end
    end
    accuracy_rec(i) = max_accuracy;
    features_train = [features_train, BE_train_norm(:, max_accuracy_index)];
    features_train_index = [features_train_index, max_accuracy_index];
    % calculate testing accuracy with current features.
    features_test = BE_test_norm(:, features_train_index);
    sig_hat_test = glmval(w_optimal, features_test, 'identity');
    y_hat_test (sig_hat_test > 0.5) = 1;
    y_hat_test (isnan(sig_hat_test)) = NaN;
    accuracy_rec_test(i) = sum(y_hat_test == titanic_test_survival) / (rows - 1 - sum(isnan(y_hat_test)));
end
figure;
subplot(2, 1, 1);
plot(1:10, accuracy_rec);
title('Training accuracy');
subplot(2,1,2);
plot(1:10, accuracy_rec_test);
title('Testing accracy');
clear('w','sig_hat','y_hat','sig_hat_test','y_hat_test','max_accuracy','max_accuracy_index');


% problem 4g
% use X_train_sub and X_test_sub
eta = [0.01, 0.001, 1e-4];
iterations = [10, 100, 200, 300, 400, 500,1000];
% append column of 1 to feature matrix. use only top 5 features.
features_train = [ones(length(titanic_train_survival), 1), X_train_sub];
features_test = [ones(length(titanic_test_survival), 1), X_test_sub];
figure;
for i = 1 : length(eta)
    str = strcat('eta = ' , num2str(eta(i)));
    accuracy_iter = zeros(1, 7);
    for j = 1 : length(iterations)
        count = 0;
        w = ones(1, 9);
        while (count <= iterations(j))
           % w * transpose(X). 1 x 655 matrix. 
           alpha = w * transpose(features_train);
           sig = (1 + exp(-alpha)) .^ (-1);
           % 655 x 1 vector
           sig_minus_y = transpose(sig) - titanic_train_survival;
           % 655 x 11 matrix with repeating columns
           sig_minus_y = repmat(sig_minus_y, 1, 9);
           % sum across all samples. grad is 1 * 11 matrix
           grad = sum(sig_minus_y .* features_train, 1);
           %w_t = w - eta * w
           w = w - eta(i) * grad;
           count = count + 1;
        end
        sig_hat = glmval(transpose(w), X_train_sub,'identity');
        y_hat = zeros(length(titanic_train_survival), 1);
        y_hat (sig_hat > 0.5) = 1;
        accuracy_iter(j) = sum(y_hat == titanic_train_survival) / length(titanic_train_survival);
    end
    subplot(3, 1, i);
    plot(iterations, accuracy_iter);
    title(str);
    
    w = ones(1, 9);
    count = 0;
    epsilon = 1;
    while (epsilon > 1e-4 && count < 10000)
           % w * transpose(X). 1 x 655 matrix. 
           alpha = w * transpose(features_train);
           sig = (1 + exp(-alpha)) .^ (-1);
           % 655 x 1 vector
           sig_minus_y = transpose(sig) - titanic_train_survival;
           % 655 x 11 matrix with repeating columns
           sig_minus_y = repmat(sig_minus_y, 1, 9);
           % sum across all samples. grad is 1 * 11 matrix
           grad = sum(sig_minus_y .* features_train, 1);
           %w_t = w - eta * w
           w = w - eta(i) * grad;
           epsilon = sqrt(sum(grad .^ 2)); 
           count = count + 1;
    end
    str = strcat(str, ': converges after ', num2str(count) , ' iterations.');
    fprintf('%s\n', str);
end










        
