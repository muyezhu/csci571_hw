function [data_dummy] = preprocess(data, dummy_col)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    size_data_dummy = 2 * size(dummy_col, 2) + size(data, 2);   % # of cols in new dummy coded data
    data_dummy = zeros(size(data, 1), size_data_dummy);
    dummy_col_i = 1;
    data_dummy_i = 1;
    for i = 1 : size(data, 2)
        if i == dummy_col(min(dummy_col_i, size(dummy_col, 2)))
           temp = double(data(:, i) + 2); 
           data_dummy(:, data_dummy_i : (data_dummy_i + 2)) = dummyvar(temp);
           dummy_col_i = dummy_col_i + 1;
           data_dummy_i = data_dummy_i + 3;
        else
           data_dummy(:, data_dummy_i) = data(:, i);
           data_dummy_i = data_dummy_i + 1;
        end  
    end
    % feature 16 has values {-1, 1} and was mapped to categories {1, 2, 3}
    % under dummyvar(group). From the final data_dummy, remove the center
    % column of feature 16 dummy code. In data_dummy, this corresponds to
    % column 27.
    data_dummy = [data_dummy(:, 1 : 26), data_dummy(:, 28 : end)];
end

