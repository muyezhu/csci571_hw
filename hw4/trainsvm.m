function [w, b, e] = trainsvm(features, label, C)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    d = size(features, 2);
    n = size(features, 1);
    % input vector u to quadratic programming: u = [w; b; e]
    w = zeros(d, 1);
    b = zeros(1, 1);
    e = zeros(n, 1);
    u = [w; b; e];
    % H and f
    H = zeros(size(u, 1));
    H(1 : d, 1 : d) = eye(d);
    f = [zeros(d + 1, 1); ones(n, 1) * C];
    % A for constraints
    % Two sets of constraints: (1) yn * [W'xn+ b] + en >= 1
    % (2) en >= 0
    design_mat = repmat(label, 1, d) .* features;
    b_I = label;
    A1 = [design_mat, b_I, eye(n)];
    A2 = [zeros(n, d), zeros(n, 1), eye(n)];
    A = -[A1; A2];
    % Ax <= c. The above two unequality is written as -Ax <= -1 and -Ax <=0
    c1 = ones(n, 1) * (-1);
    c2 = zeros(n, 1);
    c = [c1; c2];
    % options
    opts = optimoptions('quadprog','Algorithm', 'interior-point-convex', 'Display', 'off');
    %opts = optimoptions('quadprog','Algorithm', 'interior-point-convex', 'Diagnostics', 'on', 'Display','iter');
    %opts = optimoptions('quadprog','Algorithm', 'active-set');
    x = quadprog(H, f, A, c, [], [], [], [], [], opts);
    w = x(1 : d);
    b = x(d + 1);
    e = x(d + 2 : end);
end

