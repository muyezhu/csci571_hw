% 3.1 data preprocessing
dummy_col = [2, 7, 8, 14, 15, 16, 26, 29];
load('phishing-train.mat');
label_train = double(transpose(label));
features_train = preprocess(features, dummy_col);
load('phishing-test.mat');
label_test = double(transpose(label));
features_test = preprocess(features, dummy_col);
clear('dummy_col', 'label', 'features');

% 3.2
[w, b, e] = trainsvm(features_train, label_train, 1 / 16);
test_accu = testsvm(features_test, label_test, w, b);


a = -6 : 1 : 2;
C = power(4, a);
cv_size = floor(size(label_train, 1) / 3);
accu_C = zeros(size(C, 2), 1);
t_C = zeros(size(C, 2), 1);
% 3.3a
for i = 1 : size(C, 2)
   % cv 1
   tic;
   cv_features_train = features_train(1 : 2 * cv_size, :);
   cv_label_train = label_train(1 : 2 * cv_size);
   cv_features_test = features_train(2 * cv_size + 1 : end, :);
   cv_label_test = label_train(2 * cv_size + 1 : end);
   [w, b] = trainsvm(cv_features_train, cv_label_train, C(i));
   accu_C(i) = accu_C(i) + testsvm(cv_features_test, cv_label_test, w, b);
   % cv 2
   cv_features_train = features_train(cv_size + 1 : end, :);
   cv_label_train = label_train(cv_size + 1 : end);
   cv_features_test = features_train(1 : cv_size, :);
   cv_label_test = label_train(1 : cv_size);
   [w, b] = trainsvm(cv_features_train, cv_label_train, C(i));
   accu_C(i) = accu_C(i) + testsvm(cv_features_test, cv_label_test, w, b);
   % cv 3
   cv_features_train = features_train([1 : cv_size, 2 * cv_size + 1 : end], :);
   cv_label_train = label_train([1 : cv_size, 2 * cv_size + 1 : end]);
   cv_features_test = features_train(cv_size + 1 : 2 * cv_size, :);
   cv_label_test = label_train(cv_size + 1 : 2 * cv_size);
   [w, b] = trainsvm(cv_features_train, cv_label_train, C(i));
   accu_C(i) = accu_C(i) + testsvm(cv_features_test, cv_label_test, w, b);
   t_C(i) = toc;
end
accu_C = accu_C / 3;
t_C = t_C / 3;
figure;
subplot(2, 1, 1);
plot(a, accu_C);
title('CV test accuracy');
xlabel('log4(C)');
subplot(2, 1, 2);
plot(a, t_C);
title('CV training time');
xlabel('log4(C)');

% 3.3c
C_optimal = 4^-2;
own_linear;

% 3.4 linear kernel
cv_accu_libsvm = zeros(size(C, 2), 1);
t_cv_libsvm = zeros(size(C, 2), 1);
for i = 1 : size(C, 2)
    options = ['-q -v 3 -b 0 -t 0 -s 0 -c ', num2str(C(i))];
    tic;
    cv_accu_libsvm(i) = svmtrain(label_train, features_train, options);
    t_cv_libsvm(i) = toc / 3;
end
figure;
subplot(2, 1, 1);
plot(a, cv_accu_libsvm);
title('linear kernel libsvm cv accuracy');
xlabel('log4(C)');
subplot(2, 1, 2);
plot(a, t_cv_libsvm);
xlabel('log4(C)');
[M, C_libsvm_linear_i] = max(cv_accu_libsvm);
C_libsvm_linear = C(C_libsvm_linear_i);
fprintf('Optimal c value in libsvm linear kernel cross validation is %f\n', C_libsvm_linear);


% 3.5a polynomial kernel
fprintf('libsvm polynomial kernel\n');
d = [1, 2, 3];
a = -3 : 1 : 7;
C = power(4, a);
cv_accu_poly = zeros(size(C, 2), size(d, 2));
t_cv_poly = zeros(size(C, 2),size(d, 2));
C_poly  =1;
d_poly = 1;
m = 0;
for i = 1 : size(C, 2)
    for j = 1 : size(d, 2)
        options = ['-q -v 3 -b 0 -t 1 -s 0 -c ', num2str(C(i)), ' -d ', num2str(d(j))];
        tic;
        cv_accu_poly(i, j) = svmtrain(label_train, features_train, options);
        t_cv_poly(i, j) = toc / 3;
        fprintf('C = %f, d = %d, cv training accuracy = %f, cv average training time = %f\n', C(i), d(j), cv_accu_poly(i, j), t_cv_poly(i, j));
        if cv_accu_poly(i, j) > m
           C_poly_i = i;
           d_poly_j = j;
           m = cv_accu_poly(i, j);
        end
        if cv_accu_poly(i, j) == m
           if t_cv_poly(i, j) < t_cv_poly(C_poly_i, d_poly_j)
              C_poly_i = i;
              d_poly_j = j;
           end
        end
    end
end
fprintf('Optimal parameters for polynomial kernel svm are C = %f, d = %d\n', C(C_poly_i), d(d_poly_j));

% 3.5b
fprintf('libsvm rbf kernel\n');
g = -7 : 1 : -1;
gamma = power(4, g);
cv_accu_rbf = zeros(size(C, 2), size(gamma, 2));
t_cv_rbf = zeros(size(C, 2),size(gamma, 2));
C_rbf = 1;
gamma_rbf = 1;
m = 0;
for i = 1 : size(C, 2)
    for j = 1 : size(gamma, 2)
        options = ['-q -v 3 -b 1 -t 2 -s 0 -c ', num2str(C(i)), ' -g ', num2str(gamma(j))];
        tic;
        cv_accu_rbf(i, j) = svmtrain(label_train, features_train, options);
        t_cv_rbf(i, j) = toc / 3;
        fprintf('C = %f, gamma = %f, cv training accuracy = %f, cv average training time = %f\n', C(i), gamma(j), cv_accu_rbf(i, j), t_cv_rbf(i, j));
        if cv_accu_rbf(i, j) > m
           C_rbf_i = i;
           gamma_rbf_j = j;
           m = cv_accu_rbf(i, j);
        end
        if cv_accu_rbf(i, j) == m
           if t_cv_rbf(i, j) < t_cv_rbf(C_rbf_i, gamma_rbf_j)
              C_rbf_i = i;
              gamma_rbf_j = j;
           end
        end
    end
end
fprintf('Optimal parameters for rbf kernel svm are C = %f, gamma = %d\n', C(C_rbf_i), gamma(gamma_rbf_j));

C = 1;
gamma = 0.0625;
fprintf('Optimal kernel is rbf  with C = %f and gamma = %f\n', C, gamma);
% train with best kernel optimal params. test for accuracy.
libsvm;


















