function [test_accu, predict] = testsvm(features, label, w, b)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    predict = sign(features * w + b);
    test_accu = sum(predict == label, 1) / size(label, 1);
end

