[w, b, e] = trainsvm(features_train, label_train, C_optimal);
test_accu = testsvm(features_test, label_test, w, b);
fprintf('The C value %f was chosen from cross validation because it has highest cross validation training accuracy. \nTesting accuracy with C = %f is %f.\n', C_optimal, C_optimal, test_accu);
