% first 911 samples
data = csvread('train.csv',1,0,[1,0,9621,23]);
% cols where empty cells can occur
data_trim = data(:, 4 : end - 1);
sum_trim = sum(data_trim, 2);
% remove rows where all features are empty. cols 4 : 23 are considered
% features.
data_minus_empty_rows = data(sum_trim > 0,:);
% normalize features and observations
mean_dmer = nanmean(data_minus_empty_rows(:, 4 : end), 1);
mean_dmer_mat = repmat(mean_dmer, size(data_minus_empty_rows, 1), 1);
std_dmer = nanstd(data_minus_empty_rows(:, 4 : end),1);
std_dmer = repmat(std_dmer, size(data_minus_empty_rows, 1), 1);
dmer_norm = (data_minus_empty_rows(:, 4 : end) - mean_dmer_mat) ./ std_dmer;
dmer_norm = [data_minus_empty_rows(:, 1 : 3), dmer_norm];
% plot density estimation of rainfall observation for each remaining id: gaussian kernal
[uni_id, i_uni, i_id] = unique(data_minus_empty_rows(:,1));
rainfall = data_minus_empty_rows(i_uni, 24);
rainfall_sorted = sort(rainfall);
h = [1, 0.5, 0.25, 0.2, 0.1, 0.05];
figure;
for i = 1 : size(h, 2)
    fit_gaussian = gaussian_density_estimation(rainfall_sorted, rainfall_sorted, h(i));
    subplot(3, 2, i);
    plot(rainfall_sorted, fit_gaussian);
    % do not display rainfall extreme values.
    axis([0 200 0 0.5])
    str= strcat('gaussian kernel, h = ', num2str(h(i)));
    title(str);
end

