function [new_accu, train_accu, new_predict, train_predict] = knn_classify(train_data, train_label, new_data, new_label, K)
% k-nearest neighbor classifier
% Input:
%  train_data: N*D matrix, each row as a sample and each column as a
%  feature
%  train_label: N*1 vector, each row as a label
%  new_data: M*D matrix, each row as a sample and each column as a
%  feature
%  new_label: M*1 vector, each row as a label
%  K: number of nearest neighbors
%
% Output:
%  new_accu: accuracy of classifying new_data
%  train_accu: accuracy of classifying train_data (using leave-one-out
%  strategy)
%
% CSCI 567: Machine Learning, Fall 2015, Homework 1
n_train_sample = size(train_data, 1);
n_new_sample = size(new_data, 1);
% distance dimension: n_new_sample x n_train_sample
% distance(i, j): L2 distance between new_data(i) and train_data(j)
tt_distance = l2_distance(train_data, train_data);
tn_distance = l2_distance(train_data, new_data);
% train predict. disregard distance between self, which is 0.
[sorted_tt_distance, sorted_tt_indice] = sort(tt_distance, 2);
train_accu = zeros(size(K, 2), 1);
train_predict = zeros(n_train_sample, size(K, 2));
for i = 1 : size(K, 2)
    knn_tt_indice = sorted_tt_indice(:, 2 : K(i) + 1);
    train_predict(:, i) = mode(train_label(knn_tt_indice), 2);
    train_accu(i) = sum(train_predict(:, i) == train_label, 1) / n_train_sample;
end
% new data predict.
[sorted_tn_distance, sorted_tn_indice] = sort(tn_distance, 2);
new_accu = zeros(size(K, 2), 1);
new_predict = zeros(n_new_sample, size(K, 2));
for i = 1 : size(K, 2)
    knn_tn_indice = sorted_tn_indice(:, 1 : K(i));
    new_predict(:, i) = mode(train_label(knn_tn_indice), 2);
    new_accu(i) = sum(new_predict(:, i) == new_label, 1) / n_new_sample;
end



