function [distance] = l2_distance(data_a, data_b)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    n_a = size(data_a, 1);
    n_b = size(data_b, 1);
    distance = repmat(sum(data_b .^ 2, 2), 1, n_a) + repmat(sum(transpose(data_a .^ 2), 1), n_b,1) - 2 * data_b * transpose(data_a); 
end

