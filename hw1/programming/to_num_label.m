function [label_num] = to_num_label(nursery, col, varargin)
%UNTITLED12 Summary of this function goes here
%   Detailed explanation goes here
    n_cat = size(varargin, 2);
    n_sample = size(nursery{1}, 1);
    nursery_col = repmat(nursery{col}, 1, n_cat);
    nursery_col_ref = repmat(varargin, n_sample, 1);
    cmp = strcmp(nursery_col, nursery_col_ref);
    label_num = zeros(n_sample, 1);
    for i = 1 : n_cat
        label_num = label_num + cmp(:, i) * i;
    end
    assert(prod(label_num > 0) > 0);
end

