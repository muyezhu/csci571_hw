function [ fit ] = gaussian_density_estimation( test, train, h )
%UNTITLED2 Summary of this function goes here
% fit data with gaussian kernel with bandwidth h
%   Detailed explanation goes here
    n_test = size(test, 1);
    n_train = size(train, 1);
    test_mat = repmat(transpose(test), n_train, 1);
    train_mat = repmat(train, 1, n_test);
    fit = nanmean((1 / (h* sqrt(2 * pi))) * exp(-((test_mat - train_mat).^ 2)./(2 * h * h)), 1);
end

