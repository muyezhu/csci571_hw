function [ fit ] = epanechnikov_density_estimation(test, train, h)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    n_test = size(test, 1);
    n_train = size(train, 1);
    test_mat = repmat(transpose(test), n_train, 1);
    train_mat = repmat(train, 1, n_test);
    one_mat = ones(n_train, n_test);
    fit = nanmean((3 / (4* h)) * (one_mat - ((test_mat - train_mat) ./ h) .^ 2) .* (abs((test_mat - train_mat) / h) < 1), 1);
end

