function [new_accu, train_accu, P_xi_yk, train_predict] = naive_bayes(train_data, train_label, new_data, new_label)
% naive bayes classifier
% Input:
%  train_data: N*D matrix, each row as a sample and each column as a
%  feature
%  train_label: N*1 vector, each row as a label
%  new_data: M*D matrix, each row as a sample and each column as a
%  feature
%  new_label: M*1 vector, each row as a label
%
% Output:
%  new_accu: accuracy of classifying new_data
%  train_accu: accuracy of classifying train_data 
%
% CSCI 567: Machine Learning, Fall 2015, Homework 1
n_train_sample = size(train_data, 1);
n_feature = size(train_data, 2);
label = unique(train_label);
n_label = size(label, 1);
% Decision rule: k = argmax P(X|y = k)P(y = k) = argmax logP(X|y=k) +logP(y= k)
% train probabilities
P_xi_yk =  zeros(n_label, n_feature);
P_yk = zeros(n_label, 1);
for i = 1 : n_label
    sum_yk = sum(train_label == label(i), 1);
    P_yk(i) = sum_yk / n_train_sample;
    P_xi_yk(i, :) = sum(train_data .* repmat(train_label == label(i), 1, n_feature), 1) / sum_yk;
end
% check if test set contains feature not exisiting in training set. reset
% these probabilities from 0 to a small positive value.
feature_present = sum(train_data, 1);
for i = 1 : n_feature
   if feature_present(i) == 0
      P_xi_yk(:, i) = ones(n_label, 1) * 0.1; 
   end
end
train_predict = bayes_predict(P_xi_yk, P_yk, train_data, label);
train_accu = sum(train_predict == train_label, 1) / n_train_sample;
new_predict = bayes_predict(P_xi_yk, P_yk, new_data, label);
new_accu = sum(new_predict == new_label, 1) / size(new_data, 1);









