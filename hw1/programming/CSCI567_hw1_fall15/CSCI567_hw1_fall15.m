% 5.1
load 'hw1progde.mat';
[x_tr_sorted, x_tr_sorted_indices] = sort(x_tr);
h = [0.5, 0.25, 0.2, 0.1, 0.05, 0.025];
% 5.1a1. fit x_tr with Gaussian kernel
figure;
for i = 1 : size(h, 2)
    fit_gaussian = gaussian_density_estimation(x_tr_sorted, x_tr_sorted, h(i));
    subplot(3, 2, i);
    plot(x_tr_sorted, fit_gaussian);
    str= strcat('gaussian kernel, h = ', num2str(h(i)));
    title(str);
end
% 5.1a2 fit x_tr with Epanechnikov kernel
figure;
for i = 1 : size(h, 2)
    fit_epanechnikov = epanechnikov_density_estimation(x_tr_sorted, x_tr_sorted, h(i));
    subplot(3, 2, i);
    plot(x_tr_sorted, fit_epanechnikov);
    str= strcat('epanechnikov kernel, h = ', num2str(h(i)));
    title(str);
end
% 5.1a3 fit x_tr with histogram
figure;
for i = 1 : size(h, 2)
    fit_histogram = histogram_density_estimation(x_tr_sorted, x_tr_sorted, h(i));
    subplot(3, 2, i);
    plot(x_tr_sorted, fit_histogram);
    str= strcat('histogram, h = ', num2str(h(i)));
    title(str);
end
% 5.2
% 50 evenly spaced points in [0 , 1] to be evaluated
stepsize = 1 / 49;
x = transpose(0: stepsize :1);
n = 19;
size_n = 500;
X = zeros(n, size(x, 1));
integral = zeros(size(h,2), 1);
p = randperm(size(x_te, 1));
x_te = x_te(p);
% test gaussian kernel
for i = 1 : size(h, 2)
    for j = 1 : n
        X(j, :) = gaussian_density_estimation(x, x_te(1 + size_n * (j - 1) : size_n * j), h(i));
    end
    integral(i) = get_integral(X, stepsize, n);
end
figure;
subplot(3, 1, 1);
plot(h, integral);
% test epanechnikov kernel
for i = 1 : size(h, 2)
    for j = 1 : n
        X(j, :) = epanechnikov_density_estimation(x, x_te(1 + size_n * (j - 1) : size_n * j), h(i));
    end
    integral(i) = get_integral(X, stepsize, n);
end
subplot(3, 1, 2);
plot(h, integral);
% test histogram
for i = 1 : size(h, 2)
    for j = 1 : n
        X(j, :) = histogram_density_estimation(x, x_te(1 + size_n * (j - 1) : size_n * j), h(i));
    end
    integral(i) = get_integral(X, stepsize, n);
end
subplot(3, 1, 3);
plot(h, integral);

clear;

% 5.2a ttt preprocess
ttt_train = preprocess_ttt('hw1ttt_train.data');
ttt_test = preprocess_ttt('hw1ttt_test.data');
ttt_valid = preprocess_ttt('hw1ttt_valid.data'); 
% 5.2a nursery preprocess
nursery_train = preprocess_nursery('hw1nursery_train.data');
nursery_test = preprocess_nursery('hw1nursery_test.data');
nursery_valid = preprocess_nursery('hw1nursery_valid.data');

% 5.2b naive bayes training
fprintf('naive bayes\n');
[ttt_test_accu, ttt_train_accu, P] = naive_bayes(ttt_train(:, 1 : (end - 1)), ttt_train(:, end), ...
                                          ttt_test(:, 1 : (end - 1)), ttt_test(:, end));
ttt_valid_accu = naive_bayes(ttt_train(:, 1 : (end - 1)), ttt_train(:, end), ...
                                          ttt_valid(:, 1 : (end - 1)), ttt_valid(:, end));
fprintf('ttt train accuracy is %f, ttt test accuracy is %f, ttt valid accuracy is %f\n', ...
        ttt_train_accu, ttt_test_accu, ttt_valid_accu);
[nursery_test_accu, nursery_train_accu] = naive_bayes(nursery_train(:, 1 : (end - 1)), nursery_train(:, end), ...
                                          nursery_test(:, 1 : (end - 1)), nursery_test(:, end));
nursery_valid_accu = naive_bayes(nursery_train(:, 1 : (end - 1)), nursery_train(:, end), ...
                                          nursery_valid(:, 1 : (end - 1)), nursery_valid(:, end));
fprintf('nursery train accuracy is %f, nursery test accuracy is %f, nursery valid accuracy is %f\n', ...
         nursery_train_accu, nursery_test_accu, nursery_valid_accu);
     
% 5.2c KNN
knn;

% 5.2d decision tree
min_leaf = 1 : 10;
n_ttt_train = size(ttt_train, 1);
n_ttt_test = size(ttt_test, 1);
n_ttt_valid = size(ttt_valid, 1);
% fit tree using gini index as split criterion, train, test, valid
gini_index_accu = zeros(3, size(min_leaf, 2));
fprintf('gini_index split\n');
for i = 1 : size(min_leaf, 2)
    tree = ClassificationTree.fit(ttt_train(:, 1 : (end - 1)), ttt_train(:, end), 'SplitCriterion', 'gdi', 'MinLeaf', min_leaf(i), 'Prune', 'off');
    tree_label_train = predict(tree, ttt_train(:, 1 : (end - 1)));
    tree_label_test = predict(tree, ttt_test(:, 1 : (end - 1)));
    tree_label_valid = predict(tree, ttt_valid(:, 1 : (end - 1)));
    gini_index_accu(1, i) = sum(tree_label_train == ttt_train(:, end), 1) / n_ttt_train;
    gini_index_accu(2, i) = sum(tree_label_test == ttt_test(:, end), 1) / n_ttt_test;
    gini_index_accu(3, i) = sum(tree_label_valid == ttt_valid(:, end), 1) / n_ttt_valid;
    fprintf('min_leaf = %d, ttt train accuracy = %f, ttt test accuracy = %f, ttt valid accuracy = %f\n', min_leaf(i), gini_index_accu(1, i),...
             gini_index_accu(2, i), gini_index_accu(3, i));
end
% fit tree using cross entropy as split criterion, train, test, valid
cross_entropy_accu = zeros(3, size(min_leaf, 2));
fprintf('cross entropy split\n');
for i = 1 : size(min_leaf, 2)
    tree = ClassificationTree.fit(ttt_train(:, 1 : (end - 1)), ttt_train(:, end), 'SplitCriterion', 'deviance', 'MinLeaf', min_leaf(i), 'Prune', 'off');
    tree_label_train = predict(tree, ttt_train(:, 1 : (end - 1)));
    tree_label_test = predict(tree, ttt_test(:, 1 : (end - 1)));
    tree_label_valid = predict(tree, ttt_valid(:, 1 : (end - 1)));
    cross_entropy_accu(1, i) = sum(tree_label_train == ttt_train(:, end), 1) / n_ttt_train;
    cross_entropy_accu(2, i) = sum(tree_label_test == ttt_test(:, end), 1) / n_ttt_test;
    cross_entropy_accu(3, i) = sum(tree_label_valid == ttt_valid(:, end), 1) / n_ttt_valid;
    fprintf('min_leaf = %d, ttt train accuracy = %f, ttt test accuracy = %f, ttt valid accuracy = %f\n', min_leaf(i), cross_entropy_accu(1, i),...
             cross_entropy_accu(2, i), cross_entropy_accu(3, i));
end

% 5.2e decision boundary
load('hw1boundary.mat');   % contains 'featres' and 'labels'
features = normalize(features);
new_features = unifrnd(0, 1, 10000, 2);
new_labels = ones(size(new_features, 1), 1);    % placeholder
K = [1, 5, 15, 25];
[a, b, predict, d] = knn_classify(features, labels, normalize(new_features), new_labels, K);
figure;
for i = 1 : size(K, 2)
    subplot(2, 2, i);
    str = strcat('k = ', num2str(K(i)));
    scatter(new_features(predict(:, i) == 1, 1), new_features(predict(:, i) == 1, 2), 1, 'r');
    hold on;
    scatter(new_features(predict(:, i) == -1, 1), new_features(predict(:, i) == -1, 2), 1, 'g');
    title(str);
end










