function [dummy] = dummy_code(nursery, col, varargin)
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here
    n_cat = size(varargin, 2);
    n_sample = size(nursery{1}, 1);
    nursery_col = repmat(nursery{col}, 1, n_cat);
    nursery_col_ref = repmat(varargin, n_sample, 1);
    dummy = strcmp(nursery_col, nursery_col_ref);
    assert(prod(sum(dummy, 2) == ones(n_sample, 1)) > 0);
end

