function [data_norm] = normalize(data)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    n_sample = size(data, 1);
    mean = nanmean(data, 1);
    std = nanstd(data, 1);
    data_norm = (data - repmat(mean, n_sample, 1)) ./ (repmat(std, n_sample, 1));
end

