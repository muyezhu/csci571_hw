function [ fit ] = histogram_density_estimation(test, train, h)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
    bin_ranges = 0 : h : 1;
    bin_count = histc(train, bin_ranges);
    p_bin = bin_count / size(train, 1);
    test_bin = floor(test / h) + 1;
    fit = p_bin(test_bin) / h;
end

