% 5.2c KNN
fprintf('KNN\n');
ttt_train(:, 1 : (end - 1)) = normalize(ttt_train(:, 1 : (end - 1)));
ttt_test(:, 1 : (end - 1)) = normalize(ttt_test(:, 1 : (end - 1)));
ttt_valid(:, 1 : (end - 1)) = normalize(ttt_valid(:, 1 : (end - 1)));
nursery_train(:, 1 : (end - 1)) = normalize(nursery_train(:, 1 : (end - 1)));
nursery_test(:, 1 : (end - 1)) = normalize(nursery_test(:, 1 : (end - 1)));
nursery_valid(:, 1 : (end - 1)) = normalize(nursery_valid(:, 1 : (end - 1)));

K = 1 : 2 : 15;
[ttt_test_accu, ttt_train_accu] = knn_classify(ttt_train(:, 1 : (end - 1)), ttt_train(:, end), ...
                                          ttt_test(:, 1 : (end - 1)), ttt_test(:, end), K);
ttt_valid_accu = knn_classify(ttt_train(:, 1 : (end - 1)), ttt_train(:, end), ...
                                          ttt_valid(:, 1 : (end - 1)), ttt_valid(:, end), K);
for i = 1 : size(K, 2)                                      
    fprintf('K = %d\n ttt train accuracy is %f, ttt test accuracy is %f, ttt valid accuracy is %f\n', ...
            K(i), ttt_train_accu(i), ttt_test_accu(i), ttt_valid_accu(i));
end
[nursery_test_accu, nursery_train_accu] = knn_classify(nursery_train(:, 1 : (end - 1)), nursery_train(:, end), ...
                                          nursery_test(:, 1 : (end - 1)), nursery_test(:, end), K);
nursery_valid_accu = knn_classify(nursery_train(:, 1 : (end - 1)), nursery_train(:, end), ...
                                          nursery_valid(:, 1 : (end - 1)), nursery_valid(:, end), K);
for i = 1 : size(K, 2)                                      
    fprintf('K =%d\n nursery train accuracy is %f, nursery test accuracy is %f, nursery valid accuracy is %f\n', ...
             K(i), nursery_train_accu(i), nursery_test_accu(i), nursery_valid_accu(i));
end