function [predict] = bayes_predict(P_xi_yk, P_yk, data, label)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    % log(P(X|y=k)) + log(P(y=k)) for all samples
    n_sample = size(data, 1);
    epsilon = 1e-6;  % epsilon is introduced to allow numeric computation with p values of 0.
    P_xi_yk (P_xi_yk == 0) = epsilon;
    P_yk (P_yk == 0) = epsilon;
    data_P_yk = data * log(transpose(P_xi_yk)) + log(repmat(transpose(P_yk), n_sample, 1));
    [M,I] = max(data_P_yk, [], 2);
    predict = label(I);
end

