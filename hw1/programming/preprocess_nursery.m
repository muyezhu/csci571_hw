function [nursery] = preprocess_nursery(file)
%UNTITLED10 Summary of this function goes here
% parents: usual, pretentious, great_pret 
% has_nurs: proper, less_proper, improper, critical, very_crit 
% form: complete, completed, incomplete, foster 
% children: 1, 2, 3, more 
% housing: convenient, less_conv, critical 
% finance: convenient, inconv 
% social: nonprob, slightly_prob, problematic 
% health: recommended, priority, not_recom
% label: spec_prior, priority, very_recom, recommend, not_recom
%   Detailed explanation goes here
    fileID = fopen(file);
    nursery = textscan(fileID, '%s %s %s %s %s %s %s %s %s', 'Delimiter', ',');
    parents = dummy_code(nursery, 1, 'usual', 'pretentious', 'great_pret');
    has_nurs = dummy_code(nursery, 2, 'proper', 'less_proper', 'improper', 'critical', 'very_crit');
    form = dummy_code(nursery, 3, 'complete', 'completed', 'incomplete', 'foster');
    children = dummy_code(nursery, 4, '1', '2', '3', 'more');
    housing = dummy_code(nursery, 5, 'convenient', 'less_conv', 'critical');
    finance = dummy_code(nursery, 6, 'convenient', 'inconv');
    social = dummy_code(nursery, 7, 'nonprob', 'slightly_prob', 'problematic');
    health = dummy_code(nursery, 8, 'recommended', 'priority', 'not_recom');
    label = to_num_label(nursery, 9, 'spec_prior', 'priority', 'very_recom', 'recommend', 'not_recom');
    nursery = [parents, has_nurs, form, children, housing, finance, social, health, label];
end

