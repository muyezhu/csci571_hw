function [ ttt_bi ] = preprocess_ttt(file)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
fileID = fopen(file);
ttt = textscan(fileID, '%c %c %c %c %c %c %c %c %c %s', 'Delimiter', ',', 'EndOfLine', '\n');
n_sample = size(ttt{1}, 1);
n_feature= size(ttt, 2);
% 1-to-3 encoding of 'x'/'o'/'b'
xob = zeros(n_sample, 3);
ttt_bi = zeros(n_sample, 3 * (n_feature - 1) + 1);
x = repmat('x', n_sample, 1);
o = repmat('o', n_sample, 1);
b = repmat('b', n_sample, 1);
for i = 1 : n_feature - 1
   xob(:, 1) = ttt{i} == x; 
   xob(:, 2) = ttt{i} == o; 
   xob(:, 3) = ttt{i} == b; 
   ttt_bi(:, 1 + (i - 1) * 3 : i * 3) = xob;
   assert(prod(sum(xob, 2) == ones(n_sample, 1)) > 0);
end
% 'positive': 1; 'negative': 0
positive = repmat({'positive'}, n_sample, 1);
observation = strcmp(positive, ttt{end}); 
ttt_bi(:, end) = observation;
end

