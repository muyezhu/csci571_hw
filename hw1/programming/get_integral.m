function [ integral ] = get_integral( X, stepsize, n )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
    X_mean = nanmean(X, 1);
    X_mean_mat = repmat(X_mean, size(X, 1), 1);
    integral = (stepsize / n) * sum(sum((X - X_mean_mat).^ 2));
end

