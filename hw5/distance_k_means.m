function [ D ] = distance_k_means(data, mu)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
    D = zeros(size(data, 1), size(mu, 1));
    for i = 1 : size(mu, 1)
        repmu = repmat(mu(i,:), size(data, 1), 1);
        D(:, i) = sum(data .^ 2, 2) + sum(repmu .^ 2, 2) - 2 * data * transpose(mu(i, :));
    end

end

