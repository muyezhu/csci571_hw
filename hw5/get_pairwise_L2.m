function [p_L2] = get_pairwise_L2(data)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
    data_t = transpose(data);
    p_L2 = repmat(sum(data .^ 2, 2), 1, size(data, 1)) + repmat(sum(data_t .^ 2, 1), size(data, 1), 1) - 2 * data * data_t;
end

