load('hw5_blob.mat');
blob = points;
load('hw5_circle.mat');
circle= points;
clear('points');

problem4b;

% EM gaussian mixture
% initialize
ll_best = -inf;
mu_best = zeros(K, size(blob, 2));
C1_best = zeros(K, K);
C2_best = zeros(K, K);
C3_best = zeros(K, K);
r_nk_best = zeros(size(blob, 1), K);
repeat = 0;
K = 3;
figure;
while repeat < 5
    mu_old = zeros(K, size(blob, 2));
    w_old = ones(K, 1) / 3;
    mu_old(1, :) = blob(randi([1 size(blob, 1)], 1, 1), :);
    mu_old(2, :) = blob(randi([1 size(blob, 1)], 1, 1), :);
    mu_old(3, :) = blob(randi([1 size(blob, 1)], 1, 1), :);
    % cov() function: covariance matrix. 2x2
    C1_old = cov(blob);
    C2_old = cov(blob);
    C3_old = cov(blob);
    mu = zeros(K, size(blob, 2));
    w = zeros(K, 1);
    r_nk = zeros(size(blob, 1), K);
    C1 = zeros(size(blob, 2), size(blob, 2));
    C2 = zeros(size(blob, 2), size(blob, 2));
    C3 = zeros(size(blob, 2), size(blob, 2));
    searching = true;
    iter = 0;
    ll_old = -inf;
    ll_list = [];
    while searching
       % E step compute  r_nk
       for i = 1 : size(blob, 1)
           px = w_old(1) * mvnpdf(blob(i, :), mu_old(1, :), C1_old) + w_old(2) * mvnpdf(blob(i, :), mu_old(2, :), C2_old) + w_old(3) * mvnpdf(blob(i, :), mu_old(3, :), C3_old);
           r_nk(i, 1) = w_old(1) * mvnpdf(blob(i, :), mu_old(1, :), C1_old) / px; 
           r_nk(i, 2) = w_old(2) * mvnpdf(blob(i, :), mu_old(2, :), C2_old) / px;
           r_nk(i, 3) = w_old(3) * mvnpdf(blob(i, :), mu_old(3, :), C3_old) / px;
       end
       % M step
       for k = 1 : K
          % update mu 
          mu(k, :) = sum(repmat(r_nk(:, k), 1, size(blob, 2)) .* blob, 1) / sum(r_nk(:, k), 1); 
          % update w
          w(k) = sum(r_nk(:, k)) / size(blob, 1);
       end
       
       % update c
       for i = 1 : size(blob, 1)
           C1 = C1 + r_nk(i, 1) * transpose(blob(i, :) - mu(1, :)) * (blob(i, :) - mu(1, :));
       end
       C1 = C1 / sum(r_nk(:, 1), 1);
       for i = 1 : size(blob, 1)
           C2 = C2 + r_nk(i, 2) * transpose(blob(i, :) - mu(2, :)) * (blob(i, :) - mu(2, :));
       end
       C2 = C2 / sum(r_nk(:, 2), 1); 
       for i = 1 : size(blob, 1)
           C3 = C3 + r_nk(i, 3) * transpose(blob(i, :) - mu(3, :)) * (blob(i, :) - mu(3, :));
       end
       C3 = C3 / sum(r_nk(:, 3), 1); 
       % total log likelihood
       iter = iter + 1;
       ll = sum(log(w(1) * mvnpdf(blob, mu(1, :), C1) + w(2) * mvnpdf(blob, mu(2, :), C2) + w(3) * mvnpdf(blob, mu(3, :), C3)), 1);
       ll_list = [ll_list, ll];
       if ll - ll_old < 1e-4
          searching = false;
          subplot(2,3,repeat + 1);
          plot(1 : iter, ll_list(1 : end));
          if ll > ll_best
             ll_best = ll; 
             mu_best = mu;
             C1_best = C1;
             C2_best = C2;
             C3_best = C3;
             r_nk_best = r_nk;
          end
       else
           mu_old = mu;
           w_old = w;
           C1_old = C1;
           C2_old = C2;
           C3_old = C3;
           ll_old = ll;
       end
   end
repeat = repeat + 1;
end
[M, A_gm] = max(r_nk_best, [], 2);
colors = ['y', 'm', 'c', 'r', 'g'];
blobx = blob(:, 1);
bloby = blob(:, 2);
figure;
for i = 1 : K
   scatter(blobx(A_gm == i), bloby(A_gm == i), 5, colors(i));
   hold on;
end
















