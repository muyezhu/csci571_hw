% 4.2   k means
K = [2, 3, 5];
colors = ['y', 'm', 'c', 'r', 'g'];
A_blob = zeros(size(blob, 1), 1);
A_circle = zeros(size(circle, 1), 1);
% k means for blob
blobx = blob(:, 1);
bloby = blob(:, 2);
figure;
for i = 1 : size(K, 2)
   A_blob = k_means(blob, K(i)); 
   subplot(2,2,i);
   for j = 1 : K(i)
       scatter(blobx(A_blob == j), bloby(A_blob == j), 5, colors(j));
       hold on;
   end
   str = strcat('K = ', num2str(K(i)));
   title(str);
end
% k means for circle
circlex = circle(:, 1);
circley = circle(:, 2);
figure;
for i = 1 : size(K, 2)
   A_circle = k_means(circle, K(i)); 
   subplot(2,2, i);
   for j = 1 : K(i)
       scatter(circlex(A_circle == j), circley(A_circle == j), 5, colors(j));
       hold on;
   end
   str = strcat('K = ', num2str(K(i)));
   title(str);
end

% kernel k means. K = 2
% use phai(x) => L2 norm (x)
K = repmat(sum(circle .^ 2, 2), 1, size(circle, 1)) .* repmat(sum(transpose(circle).^ 2, 1), size(circle,1), 1);
%L = get_pairwise_L2(circle);
%K = exp(-L/0.055);
A_circle_kernel = kernel_k_means(K, 2);
figure;
for i = 1 : 2
    scatter(circlex(A_circle_kernel == i), circley(A_circle_kernel == i), 5, colors(i));
    hold on;
end