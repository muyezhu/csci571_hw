% K: kernel matrix. k: value of k in k means
function [A] = kernel_k_means(K, k)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
    A = zeros(size(K, 1), 1);
    A_new = zeros(size(K, 1), 1);
    % counts of samples in each cluster
    c = zeros(k, 1);
    % D = d_ik
    D = zeros(size(K, 1), k);
    % index of initial center
    rk = randi([1 size(K, 1)], 2, 1);
    % initial d_ik, A, c
    for i = 1 : k
       D(:, i) = diag(K) - 2 * K(:, rk(i)) + repmat(K(rk(i), rk(i)), size(K, 1), 1); 
    end
    [M, A] = min(D, [], 2);
    % iterate
    searching = true;
    while searching
        for j = 1 : k
            for i = 1 : size(K, 1)
               D(i, j) = K(i, i) - 2 * nanmean(K(i, A == j)) + nanmean(nanmean(K(A == j, A  == j)));
            end
        end
        [M, A_new] = min(D, [], 2);
        if A_new == A
            searching = false;
        else
            A = A_new;
        end
    end
    clear('M');
end

