function [A] = k_means(data, K)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    min_vals = min(data, [], 1);
    max_vals = max(data, [], 1);
    % K mu points
    mu = zeros(K, size(data, 2));
    % Distance of each sample to the K mu values.
    D = zeros(size(data, 1), K);
    % cluster assignment of samples.
    A = zeros(size(data, 1), 1);
    oldA = zeros(size(data, 1), 1);
    % initialize randomly
    for i = 1: K
        random = rand(1, size(data, 2));
        mu(i, :) = min_vals + (max_vals - min_vals) .* random;
    end
    % calculate distance
    searching = true;
    D = distance_k_means(data, mu);
    [M, oldA] = min(D, [], 2);
    while searching
        for i = 1 : K
           mu(i, :) = nanmean(data(oldA == i, :), 1); 
        end
        D = distance_k_means(data, mu);
        [M, A] = min(D, [], 2);
        if (oldA == A)
            searching = false;
        else
            oldA = A;
        end
    end
end

